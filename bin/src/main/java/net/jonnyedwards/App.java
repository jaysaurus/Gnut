package net.jonnyedwards;

import javafx.application.Application;
import javafx.stage.Stage;
public class App extends Application
{
  public static void main(String[] args) {
    launch(args);
  }
	@Override
	public void start(Stage primaryStage) throws Exception {
		new NutLoader().init();
	}
 
	public void stop() {
	}
}

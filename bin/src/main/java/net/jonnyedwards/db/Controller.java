package net.jonnyedwards.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.Stage;
import net.jonnyedwards.db.results.*;

public class Controller
{ 
  
  private Connection con;
  private static Controller ControllerInstance;
  
  private Controller() {
    try
    {                
      this.con = 
          DriverManager
            .getConnection(
                "jdbc:sqlite:" + 
                getClass()
                .getResource("/Gnut.db")
                .toString()
                .replace("jar:","")
                .replace("file:", "")
                .replaceAll("/Gnut-[0-9]+.[0-9]+[.]*[0-9]*-jar-with-dependencies.jar!", ""));
      this.con.setAutoCommit(false); // resolves irregular behaviour between Xerial JDBC driver and SQLite
    } catch (SQLException e)
    {
      e.printStackTrace();
    }    
  }
  public static Controller getInstance() 
  {
    if (ControllerInstance == null) {
      ControllerInstance = new Controller();      
    }
    return ControllerInstance;
  }
  public class Nut {    
    private Model model = new Model();
    
    private IntegerResult countRows() throws Exception
    {
      IntegerResult result = new IntegerResult();
      try (Statement st = con.createStatement())
      {
        ResultSet rs = st.executeQuery("SELECT COUNT(rowid) as nutCount FROM nut");
        while(rs.next()) {
          result.set(rs.getInt("nutCount"));          
        }
        return result;
      } catch (SQLException e)
      {
        e.printStackTrace();
      }
      throw new Exception("Failed to return count");
    }
    private BooleanResult delete(int id) throws Exception
    {
      BooleanResult result = new BooleanResult();
      if (id > -1) {
        try (
            PreparedStatement prepare = 
              con.prepareStatement(
                "DELETE FROM nut WHERE id = ?")) 
          {            
            prepare.setDouble(1, id);      
            con.commit();
            result.set(prepare.execute());            
            return result;
          } catch (SQLException e)
          {
            e.printStackTrace();
            return result;
          }
      } else throw new Exception("ID of nut not recognised. Delete request aborted.");
    }
    private NutResult getFromId(int id) throws Exception  
    {
      NutResult result = new NutResult();
      if (id > -1) {
        try (
          PreparedStatement prepare = 
            con.prepareStatement("SELECT * FROM nut WHERE id = ? LIMIT 1")) 
        {
          prepare.setDouble(1, id);          
          ResultSet rs = prepare.executeQuery();
          while (rs.next()) 
          {
            result.set(populateNutSet(rs));
          }
          return result;
        } catch (SQLException e)
        {
          e.printStackTrace();
        }
      } 
      throw new Exception("Invalid/No ID found in query");      
    }
    private IntegerResult getTopId() throws Exception
    {
      IntegerResult result = new IntegerResult();
      try (Statement st = con.createStatement())
      {
        ResultSet rs = st.executeQuery("SELECT id FROM nut ORDER BY id DESC LIMIT 1");
        while(rs.next()) {
          result.set(rs.getInt("id"));          
        }
        return result;
      } catch (SQLException e)
      {
        e.printStackTrace();
      }
      throw new Exception("Failed to return id");      
    }
    private BooleanResult insertNew(Stage stage, int colorId) throws Exception 
    {
      BooleanResult result = new BooleanResult();
      if (stage != null) {
        try (           
            PreparedStatement prepare = 
              con.prepareStatement(                
                "INSERT INTO nut ('contents', 'locX', 'locY', 'spanX','spanY','colorId') " + 
                "VALUES ('', ?, ?, ?, ?, ?)"
                )) 
          {
            prepare.setDouble(1, stage.getX());
            prepare.setDouble(2, stage.getY());
            prepare.setDouble(3, stage.widthProperty().get());
            prepare.setDouble(4, stage.heightProperty().get()); 
            prepare.setInt(5, colorId);
            result.set(prepare.execute());
            con.commit();
            return result;
          } catch (SQLException e)
          {
            e.printStackTrace();
            return result;
          }
      } else throw new Exception("no Nut found while attempting to insert.");      
    }
    private String parseTitle(String title) {
      return           
        title              
          .replaceFirst("Gnut", "")
          .replaceFirst(": ", "");
    }
    private Model.Nut populateNutSet(ResultSet rs) throws SQLException 
    {
      Model.Nut nut = model.new Nut();                       
      nut.setId(rs.getInt("id"));
      nut.setContents(rs.getString("contents"));
      nut.setTitle(rs.getString("title"));
      nut.setLocX(rs.getDouble("locX"));
      nut.setLocY(rs.getDouble("locY"));
      nut.setHeight(rs.getDouble("spanY"));
      nut.setWidth(rs.getDouble("spanX"));
      nut.setColorId(rs.getInt("colorId"));
      return nut;
    }
    private NutResultSet selectAll() throws Exception 
    {
      NutResultSet nutResultSet = new NutResultSet();
      try (Statement st = con.createStatement()) 
      {
        ResultSet rs = st.executeQuery("SELECT * FROM nut");
        List<Model.Nut> results = new ArrayList<Model.Nut>();
        while(rs.next()) 
        {          
          results.add(populateNutSet(rs));          
        }
        nutResultSet.set(results);
        return nutResultSet;
      } catch (SQLException e) 
      {
        e.printStackTrace();
      }
      throw new Exception("Unable to retrieve nut results");
    } 
    private BooleanResult updateColor(int id, String color) throws Exception
    {
      BooleanResult result = new BooleanResult();
      if (id > -1 && color != null) {
        try (
            PreparedStatement prepare = 
              con.prepareStatement(
                  "UPDATE nut " +
                  "SET colorId = (SELECT id FROM color WHERE name = ?) " +
                  "WHERE id = ?")) 
        {
          prepare.setString(1, color);
          prepare.setInt(2, id);
          result.set(prepare.execute());
          con.commit();
          return result;
        } catch (SQLException e)
        {
          e.printStackTrace();
          return result;
        }
      } else throw new Exception("Failed to update existing nut with new color; invalid ID or color.");
    }
    private BooleanResult updateExisting(int id, Stage stage, String contents) throws Exception 
    {
      BooleanResult result = new BooleanResult();
      if (id > -1) {
        try (
            PreparedStatement prepare = 
              con.prepareStatement(
                "UPDATE nut " +
                "SET contents = ?, " +
                "locX = ?, " +
                "locY = ?, " +
                "spanX = ?, " +
                "spanY = ?, " +                 
                "title = ? " +
                "WHERE id = ?"))
          {
            prepare.setString(1, contents);
            prepare.setDouble(2, stage.getX());
            prepare.setDouble(3, stage.getY());
            prepare.setDouble(4, stage.widthProperty().get());
            prepare.setDouble(5, stage.heightProperty().get());         
            prepare.setString(6, parseTitle(stage.getTitle()));
            prepare.setInt(7, id);            
            result.set(prepare.execute());
            con.commit();
            return result;
          } catch (SQLException e)
          {
            e.printStackTrace();
            return result;
          }
      } else throw new Exception("Failed to update existing nut. ID or contents were invalid.");  
    }
    private BooleanResult updateTitle(int id, String title)  
    {
      BooleanResult result = new BooleanResult();
      if (id > -1) {
        try (
            PreparedStatement prepare = 
              con.prepareStatement(
                "UPDATE nut " +
                "SET title = ?" +
                "WHERE id = ?")){         
          prepare.setString(1, parseTitle(title));
          prepare.setInt(2, id);
          result.set(prepare.execute());
          con.commit();
        } catch (SQLException e) {
          e.printStackTrace();
          return result;
        }
      } 
      return result;
    }
    public IControllerResult exec (Action status) throws Exception { return exec(status, null, -1, null); }    
    public IControllerResult exec (Action status, int id) throws Exception { return exec(status, null, id, null); }    
    public IControllerResult exec (Action status, Stage nut) throws Exception { return exec(status, nut, -1, null); }
    public IControllerResult exec (Action status, Stage nut, int colorId) throws Exception { return exec(status, nut, colorId, null); }
    public IControllerResult exec (Action status, int id, String contents) throws Exception { return exec(status, null, id, contents); }
    public IControllerResult exec (Action status, Stage nut, int id, String contents) throws Exception 
    {
      switch(status) 
      {
        case COUNT:
          return countRows();
        case DELETE:
          return delete(id);        
        case GET_FROM_ID:
          return getFromId(id);
        case GET_TOP_ID:
          return getTopId();
        case INSERT_NEW:
          return insertNew(nut, id);
        case SELECT_ALL:
          return selectAll();
        case UPDATE_COLOR:
          return updateColor(id, contents);
        case UPDATE_EXISTING:
          return updateExisting(id, nut, contents);
        case UPDATE_TITLE:
          return updateTitle(id, contents);
        default:
          break;
      }
      return null;
    }
  }

  public class Color {
    private Model model = new Model();    
    private ColorResult getFromId(int id) throws Exception  
    {
      ColorResult result = new ColorResult();
      if (id > -1) {
        try (
          PreparedStatement prepare = 
            con.prepareStatement("SELECT * FROM color WHERE id = ? LIMIT 1")) 
        {
          prepare.setDouble(1, id);          
          ResultSet rs = prepare.executeQuery();
          while (rs.next()) 
          {
            result.set(populateColorSet(rs));
          }
          return result;
        } catch (SQLException e)
        {
          e.printStackTrace();
        }
      } 
      throw new Exception("Invalid/No ID found in query");      
    }
    private Model.Color populateColorSet(ResultSet rs) throws SQLException
    {
      Model.Color color = model.new Color();
      color.setCss(rs.getString("css"));
      color.setId(rs.getInt("id"));
      color.setName(rs.getString("name"));
      return color;
    }
    private ColorResultSet selectAll() throws Exception 
    {
      ColorResultSet colorResultSet = new ColorResultSet();
      try (Statement st = con.createStatement())
      {
        ResultSet rs = st.executeQuery("SELECT * FROM color");
        List<Model.Color> results = new ArrayList<Model.Color>();
        while(rs.next())
        {
          results.add(populateColorSet(rs));          
        }
        colorResultSet.set(results);
        return colorResultSet;
      } catch (SQLException e) 
      {
        e.printStackTrace();
      }
      throw new Exception("Unable to retrieve color results");
    }  
    public IControllerResult exec (Action status) throws Exception { return exec(status, -1); }
    public IControllerResult exec (Action status, int id) throws Exception 
    {
      switch(status) 
      {
        case GET_FROM_ID:
          return getFromId(id);
        case SELECT_ALL:
          return selectAll();
        default:
          break;
      }
      return null;
    }
  }
}

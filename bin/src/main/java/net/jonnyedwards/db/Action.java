package net.jonnyedwards.db;

public enum Action {
  COUNT,
  DELETE,
  GET_FROM_ID,
  GET_TOP_ID,  
  INSERT_NEW, 
  SELECT_ALL,
  UPDATE_COLOR,
  UPDATE_TITLE,
  UPDATE_EXISTING,
}

package net.jonnyedwards.db.results;

public class IntegerResult implements IControllerResult<Integer>
{
  Integer result = -1;
  @Override
  public void set(Integer result)
  {
    this.result = result;
  }

  @Override
  public Integer getResult()
  {    
    return result;
  }

}

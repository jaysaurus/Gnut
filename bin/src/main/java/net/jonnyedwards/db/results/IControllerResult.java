package net.jonnyedwards.db.results;

public interface IControllerResult<TResult>
{  
  public void set(TResult t);
  public TResult getResult();
}

package net.jonnyedwards.db.results;

import java.util.List;
import net.jonnyedwards.db.Model.Nut;

public class NutResultSet implements IControllerResult<List<Nut>>
{
  List<Nut> resultSet;
  @Override
  public void set(List<Nut> resultSet)
  {
    this.resultSet = resultSet;
  }

  @Override
  public List<Nut> getResult()
  {
    return resultSet;
  }
}

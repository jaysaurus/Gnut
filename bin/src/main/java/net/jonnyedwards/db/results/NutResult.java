package net.jonnyedwards.db.results;

import net.jonnyedwards.db.Model.Nut;

public class NutResult implements IControllerResult<Nut>
{
  Nut nut = null;
  @Override
  public void set(Nut nut)
  {
    this.nut = nut;    
  }
  @Override
  public Nut getResult()
  {
    return nut;
  }
}

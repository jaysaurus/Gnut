package net.jonnyedwards.db.results;

import net.jonnyedwards.db.Model.Color;

public class ColorResult implements IControllerResult<Color>
{
  Color color = null;
  @Override
  public void set(Color color)
  {
    this.color = color;
  }

  @Override
  public Color getResult()
  {
    return color;
  }

}

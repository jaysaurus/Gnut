package net.jonnyedwards.db.results;

import java.util.List;
import net.jonnyedwards.db.Model.Color;

public class ColorResultSet implements IControllerResult<List<Color>>
{
  List<Color> resultSet;
  @Override
  public void set(List<Color> resultSet)
  {
    this.resultSet = resultSet;
  }
  @Override
  public List<Color> getResult()
  {
    return resultSet;
  }

}

package net.jonnyedwards.db.results;

public class BooleanResult implements IControllerResult<Boolean> {
  private Boolean result = false;
  
  @Override
  public void set(Boolean result)
  {
    this.result = result;    
  }

  @Override
  public Boolean getResult()
  {
    return result;
  }
}

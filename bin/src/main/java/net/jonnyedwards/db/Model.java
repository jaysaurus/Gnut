package net.jonnyedwards.db;

public class Model
{
  public class Color 
  {
    private int id;
    private String name;
    private String css;
    public int getId()
    {
      return id;
    }
    public void setId(int id)
    {
      this.id = id;
    }
    public String getName()
    {
      return name;
    }
    public void setName(String name)
    {
      this.name = name;
    }
    public String getCss()
    {
      return css;
    }
    public void setCss(String css)
    {
      this.css = css;
    }
  }
  public class Nut 
  {
    private int colorId;
    private String contents;
    private double height;
    private int id;
    private double locX;
    private double locY;
    private String title;
    private double width;
    
    public double getHeight()
    {
      return height;
    }
    public void setHeight(double height)
    {
      this.height = height;
    }
    public double getWidth()
    {
      return width;
    }
    public void setWidth(double width)
    {
      this.width = width;
    }
    public int getId()
    {
      return id;
    }
    public void setId(int id)
    {
      this.id = id;
    }
    public String getContents()
    {
      return contents;
    }
    public void setContents(String contents)
    {
      this.contents = contents;
    }
    public double getLocX()
    {
      return locX;
    }
    public void setLocX(double locX)
    {
      this.locX = locX;
    }
    public double getLocY()
    {
      return locY;
    }
    public void setLocY(double locY)
    {
      this.locY = locY;
    }
    public int getColorId()
    {
      return colorId;
    }
    public void setColorId(int colorId)
    {
      this.colorId = colorId;
    }
    public String getTitle()
    {
      return title;
    }
    public void setTitle(String title)
    {
      this.title = title;
    }
  }
}

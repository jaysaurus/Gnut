package net.jonnyedwards.nutComponents;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;
import net.jonnyedwards.db.results.ColorResultSet;
import net.jonnyedwards.eventHandlers.*;
public class NutContextMenu implements INutComponent<ContextMenu>
{    
  private Controller.Color colorController;
  private Controller controller = Controller.getInstance();
  private ToggleGroup nutColor;
  private NutColorEventHandler nutColorEventHandler;
  private TitleEventHandler titleEventHandler;
  private CopyEventHandler copyEventHandler;
  private PasteEventHandler pasteEventHandler;
  
  private Menu setColorMenuItems(Menu setColorMenu) 
  {
    try
    {
      ColorResultSet colorResultSet = (ColorResultSet)colorController.exec(Action.SELECT_ALL);
      for (Model.Color color : colorResultSet.getResult()) {
        RadioMenuItem supportedColor = new RadioMenuItem(color.getName());
        supportedColor.setUserData(color);        
        supportedColor.setToggleGroup(nutColor);
        setColorMenu.getItems().add(supportedColor);
      }
    } catch (Exception e)
    {
      System.out.println("NutContextMenu failed to retrieve colors from database");
      e.printStackTrace();
    }
    return setColorMenu;
  }
  
  public NutContextMenu(
      TitleEventHandler titleEventHandler, 
      NutColorEventHandler nutColorEventHandler,
      CopyEventHandler copyEventHandler,
      PasteEventHandler pasteEventHandler) 
  {
    this.colorController = controller.new Color();
    this.nutColor = new ToggleGroup();
    this.nutColorEventHandler = nutColorEventHandler;
    this.titleEventHandler = titleEventHandler;    
    this.copyEventHandler = copyEventHandler;
    this.pasteEventHandler = pasteEventHandler;
  }
  
  @Override
  public ContextMenu build()
  {
    final ContextMenu contextMenu = new ContextMenu();
    
    MenuItem setTitle = new MenuItem("Set Title");
    setTitle.setOnAction(titleEventHandler);      
    
    Menu setColorMenu = 
        setColorMenuItems(
            new Menu("Set Color"));
    
    MenuItem copy = new MenuItem("Copy");
    copy.setOnAction(copyEventHandler);
    MenuItem paste = new MenuItem("Paste");
    paste.setOnAction(pasteEventHandler);
    
    nutColor.selectedToggleProperty().addListener(nutColorEventHandler);
    
    contextMenu.getItems().addAll(setTitle, setColorMenu, copy, paste);
    return contextMenu;
  }

  
}

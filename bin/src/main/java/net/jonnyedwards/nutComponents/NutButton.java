package net.jonnyedwards.nutComponents;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import net.jonnyedwards.eventHandlers.NutButtonEventHandler;

public class NutButton implements INutComponent<Button>{
	private NutButtonEventHandler nutButtonEventHandler;
	public NutButton(NutButtonEventHandler nutButtonEventHandler) 
	{
		this.nutButtonEventHandler = nutButtonEventHandler;
	} 
	public Button build() {
		Button button = new Button();
    button.setId("nutButton");
    button.setText("+");
    button.setOnAction(nutButtonEventHandler);
    StackPane.setAlignment(button, Pos.BOTTOM_RIGHT);
    return button;
	}
}

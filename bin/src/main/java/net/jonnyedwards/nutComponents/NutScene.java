package net.jonnyedwards.nutComponents;

import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class NutScene implements INutComponent<Scene>
{
  private Scene scene;
  public NutScene(StackPane stackPane) 
  {
    this.scene = new Scene(stackPane, 300, 250);
  }
  public Scene build()
  {        
    String pathName = 
        getClass()
          .getResource("/style.css")
          .toString()
          .replace("jar:","")
          .replace("file:", "")
          .replaceAll("/Gnut-[0-9]+.[0-9]+-jar-with-dependencies.jar!", "");
    File file = new File(pathName);  
    if (file.exists()) {        
      try
      {
        scene.getStylesheets().add(file.toURI().toURL().toExternalForm());      
      } catch (MalformedURLException e)
      {        
        e.printStackTrace();
      }  
    } else {  
       System.out.println("Could not find css file: " + pathName);  
    }
    scene.setFill(Color.AQUA);
    return scene;
  }
}

package net.jonnyedwards.nutComponents;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;
import net.jonnyedwards.db.results.ColorResult;

public class NutTextArea implements INutComponent<TextArea>
{
  
  private int id = -1;
  
  private Controller.Color colorController;
  private Controller.Nut nutController;
  private Stage stage;
  private TextArea textArea = new TextArea();
   
  
  public NutTextArea(Stage nut) 
  {
    this.stage = nut;
  }
  
  public void activateEventHandlers() 
  {
    if (id > -1) {
      textArea.focusedProperty().addListener(new ChangeListener<Boolean>() 
      {
        @Override
        public void changed(
          ObservableValue<? extends Boolean> observable, 
          Boolean oldValue, 
          Boolean newValue)
          {
            if (!newValue) 
            {
              try
              {
                nutController.exec(
                  Action.UPDATE_EXISTING, 
                  stage, 
                  id, 
                  textArea.getText());
              } catch (Exception e)
              {
                e.printStackTrace();
              }    
            }          
          }
      });
    }
  }  
  public TextArea build()
  {    
    textArea.setId("nutTextArea");
    textArea.setWrapText(true);    
    return textArea;
  }
  public void setControllers(Controller.Nut nutController, Controller.Color colorController) 
  {
    this.nutController = nutController;
    this.colorController = colorController;
  }
  public void setId(int id) 
  {
    this.id = id;
  }
  public void style(int colorId) {
    Model.Color color;
    try
    {
      color = ((ColorResult)colorController
        .exec(
          Action.GET_FROM_ID, 
          colorId)).getResult();
      if (color != null) 
      {
        textArea.setStyle(color.getCss());
        try
        {
          nutController.exec(Action.UPDATE_COLOR, id, color.getName());
        } catch (Exception e)
        {
            System.out.println("Failed to update database with new nut's color");
            e.printStackTrace();
        }
      } else throw new Exception("failed to load color for styling textarea.");
    } catch (Exception e)
    {
      e.printStackTrace();
    }

  }
}

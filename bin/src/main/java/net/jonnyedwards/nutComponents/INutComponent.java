package net.jonnyedwards.nutComponents;

public interface INutComponent<TComponent> {
	public TComponent build();
}

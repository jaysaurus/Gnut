package net.jonnyedwards;

import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;
import net.jonnyedwards.db.results.NutResultSet;

public class NutLoader
{
  private Controller controller = Controller.getInstance();
  private boolean hasNuts = false;
  private Controller.Nut nutController;  
  public void init() 
  {
    nutController = controller.new Nut();
    try
    {
      NutResultSet nutResultSet = (NutResultSet)nutController.exec(Action.SELECT_ALL);
      for (Model.Nut nut : nutResultSet.getResult()) 
      {
        if (!hasNuts) hasNuts = true;
        new Nut(
          nut.getId(),
          nut.getLocX(),
          nut.getLocY(),                                
          nut.getWidth(),
          nut.getHeight(),
          nut.getTitle(),          
          nut.getContents(),
          nut.getColorId());
      }
      if (!hasNuts) 
      {
        new Nut();
      }
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}

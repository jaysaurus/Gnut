package net.jonnyedwards;

import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;
import net.jonnyedwards.db.results.ColorResult;
import net.jonnyedwards.db.results.IntegerResult;
import net.jonnyedwards.eventHandlers.CopyEventHandler;
import net.jonnyedwards.eventHandlers.NutButtonEventHandler;
import net.jonnyedwards.eventHandlers.NutColorEventHandler;
import net.jonnyedwards.eventHandlers.PasteEventHandler;
import net.jonnyedwards.eventHandlers.TitleEventHandler;
import net.jonnyedwards.nutComponents.*;

public class Nut {		
	private Controller controller = Controller.getInstance();
	private Controller.Color colorController;
	private int colorId;
	private boolean isExistingNut = false;
	private Stage nut;
	private Controller.Nut nutController;
	private int nutId;
	private NutTextArea nutTextArea;
	private TextArea textArea;
	private String title;
	private Nut(
	    int id, 
	    Double offsetX, 
	    Double offsetY, 
	    Double width, 
	    Double height, 
	    String contents,	    
	    String title,	    
	    boolean isExistingNut,
	    int colorId)
  {
	  this.colorId = colorId;
    this.nut = new Stage();
    this.nutController = controller.new Nut();
    this.colorController = controller.new Color();
    this.nutId = id;
    this.nutTextArea = new NutTextArea(nut);
    this.textArea = nutTextArea.build();    
    this.title = title;

    
    if (contents != "") 
    {
      textArea.setText(contents);    
    }
    
    this.isExistingNut = isExistingNut;
    create();

    // calculate dimensions AFTER nodes have loaded (prevents artifacts causing size distortions)
    if (offsetX != null && offsetY != null) {
      nut.setX(offsetX);
      nut.setY(offsetY);
    }
    if (height != null && width != null) 
    {
      nut.setWidth(width);
      nut.setHeight(height);
    }
  }
	private void create() 
	{        
    if (nutId < 0) {
      try {
        nutController
          .exec(
            Action.INSERT_NEW, 
            nut, 
            colorId);  

      } catch (Exception ex) 
      {
        System.out.println("controller exception: " + ex.getMessage());      
      }   
      
      try
      {        
        IntegerResult result = (IntegerResult)nutController.exec(Action.GET_TOP_ID);
        nutId = result.getResult();
      } catch (Exception e)
      {
        e.printStackTrace();
      }      
    }
    StackPane root = new StackPane();    
    root
      .getChildren()
      .addAll(          
          textArea,                    
          new NutButton(
              new NutButtonEventHandler(
                  nutId, 
                  nut,
                  nutController)).build()          
          );

    nut.setScene(new NutScene(root).build());   
    
    try
    {
      postCreate();
    } catch (Exception ex)
    {
      System.out.println("Failed to run Post Create tasks: " + ex.getMessage());
    }
    nut.show();
	}	
	private ContextMenu createContextMenu() 
	{
      return new NutContextMenu(
          new TitleEventHandler(
              nutId,
              nutController,
              textArea, 
              nut
              ),
          new NutColorEventHandler(
              nutId,
              nutController,
              textArea),
          new CopyEventHandler(textArea),
          new PasteEventHandler(textArea)
          ).build();
  }
	private void postCreate() throws Exception 
	{
	  textArea.setContextMenu(createContextMenu());
	  nutTextArea.setControllers(nutController, colorController);
	  nutTextArea.setId(nutId);
    nutTextArea.activateEventHandlers();
    nutTextArea.style(colorId);
    nut.setTitle(
        TitleEventHandler
          .formatGUITitle(
            TitleEventHandler
              .cleanTitle(title)));
    nut.setOnCloseRequest(new EventHandler<WindowEvent>() 
      {            
        @Override
        public void handle(WindowEvent event)
        {              
          if (nut.focusedProperty().getValue()) {
            try
            {
              nutController.exec(Action.DELETE, nutId);
            } catch (Exception e)
            {
              e.printStackTrace();
            }
          }          
        }
      });
	}
	public double[] getOffset() 
  {   
    double[] offsets = {nut.getX(), nut.getY()};
    return offsets;
  }
  public double[] getSize() 
  {
    double[] size = {nut.getWidth(), nut.getHeight()};
    return size;
  }
	public boolean getIsExistingNut() 
	{
	  return isExistingNut;
	}
  public Nut() { this(-1, null, null, null, null, "", "", false, 1); } 
	public Nut(Double offsetX, Double offsetY, int colorId) { this(-1, offsetX, offsetY, null, null, "", "", false, colorId);	}
	public Nut(int id, Double offsetX, Double offsetY, Double width, Double height, String title, String contents, int colorId) {  this(id, offsetX, offsetY, width, height, contents, title, true, colorId); }		
}
package net.jonnyedwards.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;

public class TitleEventHandler implements EventHandler<ActionEvent>
{  
  private int id;
  private Controller.Nut nutController;
  private Stage stage;
  private TextArea textArea;
  
  public TitleEventHandler(
      int id, 
      Controller.Nut nutController,
      TextArea textArea, 
      Stage stage) {    
    this.id = id;
    this.nutController = nutController;
    this.stage = stage;
    this.textArea = textArea;
  }
  
  @Override
  public void handle(ActionEvent event)
  {
    String title = 
        TitleEventHandler
          .cleanTitle(
              textArea
                .getSelectedText());
    stage.setTitle(
        TitleEventHandler
          .formatGUITitle(title));
    try
    {
      nutController.exec(Action.UPDATE_TITLE, id, title);
    } catch (Exception e)
    {
      System.out.println("Failed to persist UpdateTitle action");
      e.printStackTrace();
    }
  }  
  public static String formatGUITitle(String title) {
    return title != null
        ? String
          .format(
              "Gnut%s%s", 
              title.length() > 0 
                ? ": " 
                : "", 
              title)
        : "Gnut";
  }
  public static String cleanTitle(String title) {
    return 
      title == null ? "" : (
        title.length() > 50
          ? title.substring(0,50)
          : title);
  }
}

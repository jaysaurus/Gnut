package net.jonnyedwards.eventHandlers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;

public class NutColorEventHandler implements ChangeListener<Toggle>
{
  private TextArea textArea;
  private Controller.Nut nutController;
  private int nutId;
  public NutColorEventHandler(int nutId, Controller.Nut nutController, TextArea textArea) {
    this.nutController = nutController;
    this.textArea = textArea;
    this.nutId = nutId;
  }
  
  @Override
  public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue)
  {
    if (newValue != null) {
      Toggle selectedToggle = newValue.getToggleGroup().getSelectedToggle(); 
      if (selectedToggle != null) {          
        Model.Color colorData = (Model.Color)selectedToggle.getUserData();
        try
        {
          nutController
            .exec(
              Action.UPDATE_COLOR, 
              nutId, 
              colorData.getName());                   
        } catch (Exception e)
        {
          e.printStackTrace();
        }
        textArea.setStyle(colorData.getCss());
      }
    }
  }
}

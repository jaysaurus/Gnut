package net.jonnyedwards.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class PasteEventHandler implements EventHandler<ActionEvent>
{  
  private TextArea textArea;
  
  public PasteEventHandler(TextArea textArea) {
    this.textArea = textArea;
  }
  
  @Override
  public void handle(ActionEvent event)
  {    
    final Clipboard clipboard = Clipboard.getSystemClipboard();
    if (clipboard.hasString()) {      
      textArea.paste();
    }         
  }  
}

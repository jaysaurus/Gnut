package net.jonnyedwards.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class CopyEventHandler implements EventHandler<ActionEvent>
{
  private TextArea textArea;
  
  public CopyEventHandler(TextArea textArea) {
    this.textArea = textArea;
  }
  
  @Override
  public void handle(ActionEvent event)
  {
    String text = textArea.getSelectedText();
    final Clipboard clipboard = Clipboard.getSystemClipboard();
    final ClipboardContent content = new ClipboardContent();
    content.putString(text);    
    clipboard.setContent(content);      
  }  
}

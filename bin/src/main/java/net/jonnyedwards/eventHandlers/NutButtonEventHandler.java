package net.jonnyedwards.eventHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import net.jonnyedwards.Nut;
import net.jonnyedwards.db.Action;
import net.jonnyedwards.db.Controller;
import net.jonnyedwards.db.Model;
import net.jonnyedwards.db.results.NutResult;

public class NutButtonEventHandler implements EventHandler<ActionEvent> 
{
  private int callingNutId;
  private Stage stage;
  private Controller.Nut nutController;
  
  public NutButtonEventHandler(int callingNutId, Stage stage, Controller.Nut nutController) 
  {
    this.callingNutId = callingNutId;
    this.stage = stage;
    this.nutController = nutController;
  }
  
  @Override
  public void handle(ActionEvent event)
  {
    try
    {      
       Model.Nut callingNut = (callingNutId > -1) 
         ? callingNut = 
            ((NutResult)nutController
                .exec(
                    Action.GET_FROM_ID, 
                    callingNutId)).getResult()
         : null;      
      new Nut(
          stage.getX() + 50, 
          stage.getY() + 50, 
          (callingNut != null)
          ? callingNut.getColorId()
          : 1);
    } catch (Exception e)
    {
      System.out.println("failed to retrieve calling nut's color.");
      e.printStackTrace();
    }      
  }

}

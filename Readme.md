# Gnut
---
## No Frills Sticky Notes
**written in JavaFX**

A simple, cross-OS sticky notes programme.

![Gnut Screenshot](./Screenshot1.png)

Features basic tools via the right-click menu.

I'm re-releasing this programme here with support for copy/paste since my colleagues and I still find it useful to this day!

**Title your Gnuts**

![Gnut Screenshot](./Screenshot3.png)

**Colour your Gnuts**

![Gnut Screenshot](./Screenshot2.png)

(the innuendo never gets old.)

and, as of 1.5, copy paste is now available visually (of course, ctrl-c/ctrl-v still works just as well!)

---
## How to install

1. Make sure you have at least Java8 and JavaFX installed

2. Download [The .tar.gz File](./Gnut-1.5.tar.gz) from this project page

3. store it somewhere you can access it (e.g. your home directory)

4. point your .bashrc at the executable ./Gnut file in the base directory of the program

---
## How to use

1. Run the program from terminal; make sure you keep it running in the foreground (or that you know how to find it should you run it in the backgroun).

2. Add Gnuts by clicking the + button in the corner of the initial Gnut.

3. Delete Gnuts (permanently) by closing them with the x at the top right of the window.

4. Close ALL Gnuts (maintaining them in their present state) by terminating the programme from the command line (e.g. in Bash with ctrl+C)

---

## Caveats

JavaFX behaves a little differently in different operating systems, so do consider the following:

- Some Operating Systems will allow you to maintain state and close by right -clicking the program's icon in the OS hot bar and selecting Close/Exit.  Experiment with these methods of closing the software before using this software in earnest to ensure Gnuts don't go missing.  Ultimately it is always safest to close Gnut via the terminal.

- Gnuts sometimes move a little or change size a little between sessions. However, data always seems to persist without trouble in my experience (tested Ubuntu 14/16, Lubuntu, Fedora)
